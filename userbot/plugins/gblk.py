"""Enable Seen Counter in any message,
to know how many users have seen your message
Syntax: .gblk as reply to any message"""
from telethon import events
from userbot.utils import admin_cmd


@borg.on(admin_cmd(pattern="gblk"))
async def _(event):
    if event.fwd_from:
        return
    if Config.GBLK is None:
        await event.edit("Please set the required environment variable `GBLK` for this plugin to work")
        return False
    try:
        e = await borg.get_entity(int(Config.GBLK))
    except Exception as e:
        await event.edit(str(e))
    else:
        re_message = await event.get_reply_message()
        fwd_message = await borg.forward_messages(
            e,
            re_message,
            silent=False
        )
        await borg.forward_messages(
            event.chat_id,
            fwd_message
        )
        await event.delete()
